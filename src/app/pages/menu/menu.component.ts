import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {$} from 'jquery';

declare const $;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    $(window).scroll(function () {
      const height = $(window).scrollTop();

      if (height > 110) {
        $('.navbar-expand-md').addClass('fixed-top');
      } else {
        $('.navbar-expand-md').removeClass('fixed-top');
      }

    });
  }

}
